import argparse
from connect_spark import ConnectSpark
from train import KModel

def main(args):
    sparkc = ConnectSpark(args)
    sparkc.read_db(args.in_table, args.url)
    
    kmodel = KModel()
    kmodel.train_clustering_model(sparkc.df)
    results = kmodel.get_results()
    
    sparkc.write(result, args.out_table, args.url)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', type=str, required=True)
    parser.add_argument('--in_table', type=str, default='openfoodfacts')
    parser.add_argument('--out_table', type=str, default='clust_res')
    main(parser.parse_args())
