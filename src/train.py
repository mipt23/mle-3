from pyspark.ml.clustering import KMeans
from connect_spark import ConnectSpark
import configparser

class KModel:
    def __init__(self, connect_spark_obj, k_num = 5):
        self.connect_spark_obj = connect_spark_obj
        self.k_num = k_num

    def train_clustering_model(self):
        self.k_means = KMeans(self.k, featuresCol='features', predictionCol='prediction')
        self.model = self.k_means.fit(self.df)

    def get_results(self):
        return self.model.summary.predictions[['calcium_100g', 'fat_100g', 'proteins_100g', 'energy_100g', "cluster"]]
